#ifndef __VERSION_TRACKER_HH__
#define __VERSION_TRACKER_HH__ 1

#include <vector>
#include <string>
#include <stdint.h>
class VersionTracker {
public:
  VersionTracker(){};
  std::vector<uint32_t> GetNumericVer(){return nVersion;};
  std::string           GetHumanVer()  {return sVersion;};
  std::string           GetRepositoryURL(){return repositoryURL;};
private:
  std::vector<uint32_t> nVersion;
  std::string sVersion;
  std::string repositoryURL;
};
#endif
