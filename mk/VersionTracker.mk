VERSION_FILE?=src/Version.cc
FULL_CLASS_NAME?=UNSET_NAME

N_VERSION="$(FULL_CLASS_NAME)::nVersion = "
S_VERSION="$(FULL_CLASS_NAME)::sVersion = \""
URL="$(FULL_CLASS_NAME)::repositoryURL = \""

$(VERSION_FILE): .git
	@rm -f $(VERSION_FILE) >& /dev/null
	@mkdir -p $(dir $@)	
	@echo ${N_VERSION} $(shell git rev-parse --verify HEAD | awk 'BEGIN{line="";comma=""}{for (i=0;i<length($$0);i+=8){line =line comma " 0x" substr($$0,i,8);comma=","}}END{print "{" line "}"}') ";" >  ${VERSION_FILE}
	@echo ${S_VERSION}"$(shell git describe  --dirty --always --tags  )""\";" >> ${VERSION_FILE}
	@echo ${URL}"$(shell git ls-remote --get-url)""\";" >> ${VERSION_FILE}


#$(VERSION_FILE): .svn
#	rm -f $(VERSION_FILE) >& /dev/null
#	@echo ${N_VERSION} $(shell git rev-parse --verify HEAD | awk 'BEGIN{line="";comma=""}{for (i=0;i<length($$0);i+=8){line =line comma " 0x" substr($$0,i,8);comma=","}}END{print "{" line "}"}') ";" >  ${VERSION_FILE}
#	@echo ${S_VERSION}"$(shell git describe  --dirty --always --tags  )""\";" >> ${VERSION_FILE}
#	@echo ${URL} "$(shell git ls-remote --get-url)""\";" >> ${VERSION_FILE}


clean_version:
	@rm -f $(VERSION_FILE)
